task default -depends DeployFrontend

Task DeployFrontend -description "Build and Push frontend docker image to registry" {
    Push-Location -Path "./Frontend"

	exec { docker build -t "webparser_frontend" . }

    # Пушим с версией
    exec { docker tag "webparser_frontend" "novopashinmm/webparser_frontend" }
	exec { docker login --username $parameters.username --password $parameters.pass }
    exec { docker push "novopashinmm/webparser_frontend" }
	
	exec { docker pull "docker.io/novopashinmm/webparser_frontend" }
	if ($(docker ps -a -f name=webparser_frontend -q)) {
		exec { docker stop $(docker ps -a -f name=webparser_frontend -q) }
		exec { docker rm $(docker ps -a -f name=webparser_frontend -q) }
	}
	exec { docker-compose up -d --build }

    Pop-Location
}