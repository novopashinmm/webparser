﻿export class User {
  id: number;
  unique_name: string;
  nameIdentifier: string;
  password: string;
  firstName: string;
  lastName: string;
}
