﻿import { Component } from '@angular/core';
import { first } from 'rxjs/operators';
import { ParserService } from '@app/_services';

import { User } from '@app/_models';
import { AuthenticationService } from '@app/_services';


@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    user: User;
    result: string;

    constructor(private authService: AuthenticationService,
                private parserService: ParserService) {
      this.user = this.authService.currentUserValue;
    }

    submit() {
      this.parserService.runParser().subscribe(x => this.result = x.toString());
    }
}
