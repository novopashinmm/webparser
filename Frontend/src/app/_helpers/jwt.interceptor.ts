import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '@app/_services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const currentJwt = this.authenticationService.currentJwtValue
        if (currentJwt && currentJwt.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentJwt.token}`
                }
            });
        }

        return next.handle(request);
    }
}
