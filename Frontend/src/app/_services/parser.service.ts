import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';

const API_URL = '/api/parser/run';

@Injectable({ providedIn: 'root' })
export class ParserService {

  constructor(
    private http: HttpClient
  ) {
  }

  runParser(): Observable<Object> {
    return this.http.get(API_URL);
  }
}
