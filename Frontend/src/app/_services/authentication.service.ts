﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { User } from '@app/_models';
import {Jwt} from '@app/_models/jwt';
import * as jwt_decode from "jwt-decode";

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentJwtSubject: BehaviorSubject<Jwt>;
    public currentToken: Observable<Jwt>;
    public currentUserSubject: BehaviorSubject<User>;

    constructor(private http: HttpClient) {
        this.currentJwtSubject = new BehaviorSubject<Jwt>(JSON.parse(localStorage.getItem('jwt')));
        this.currentUserSubject = new BehaviorSubject<User>(this.getDecodedAccessToken(this.currentJwtSubject.value ? this.currentJwtSubject.value.token : null));
        this.currentToken = this.currentJwtSubject.asObservable();

        this.currentToken.subscribe(token => {
            this.currentUserSubject.next(this.getDecodedAccessToken(token ? token.token : null));
        });
    }

    private getDecodedAccessToken(token: string): User {
      try {
        return jwt_decode(token);
      } catch (e) {
        return null;
      }
    }

    public get currentJwtValue(): Jwt {
      return this.currentJwtSubject.value;
    }

    public get currentUserValue(): User {
      return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/auth/login`, { username, password })
            .pipe(map(jwt => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('jwt', JSON.stringify(jwt));
                this.currentJwtSubject.next(jwt);
                return jwt;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('jwt');
        this.currentJwtSubject.next(null);
    }
}
