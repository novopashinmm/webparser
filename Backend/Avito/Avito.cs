﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreElements;
using HtmlAgilityPack;

namespace AvitoPlugin
{
    public class Avito
    {
        /// <summary>
        /// List of hosts which can be processed by this plugin
        /// </summary>
        private readonly List<string> _validHosts;

        /// <summary>
        /// Constructor
        /// </summary>
        public Avito()
        {
            this._validHosts = new List<string>();
            this._validHosts.Add("www.avito.ru");
            this._validHosts.Add("avito.ru");
        }

        /// <summary>
        /// check if url is valid for this plugin
        /// </summary>
        /// <param name="url">Url to check for ability to process by this plugin</param>
        public bool CanProcessDomain(string url)
        {
            if (string.IsNullOrEmpty(url)) // test
                return false;

            Uri.TryCreate(url, UriKind.Absolute, out var parsed);
            if (parsed == null) return false;
            var host = parsed.GetComponents(UriComponents.Host, UriFormat.Unescaped);
            return !string.IsNullOrEmpty(host) && _validHosts.Contains(host.ToLower());
        }

        public static IEnumerable<DetailShort> GetLinksFromListPage(string html, string parentUrl)
        {
            var hDoc = new HtmlDocument();
            // отдадим на обработку обработчику
            hDoc.LoadHtml(html);
            var result = new List<DetailShort>();
            var trNodesHolderList = hDoc.DocumentNode
                .Descendants("div")
                .Where(x => x.GetAttributeValue("class", "") == "js-catalog_serp");
            var nodesHolderList = trNodesHolderList as HtmlNode[] ?? trNodesHolderList.ToArray();
            if (!nodesHolderList.Any()) return result;
            {
                var trNodesHolder = nodesHolderList.First();
                var units = trNodesHolder
                    .ChildNodes
                    .Where(x => x.Name == "div");
                foreach (var node in units)
                {
                    if(node.Attributes.All(x => x.Name != "id")) continue;
                    // получим ID
                    var id = node.GetAttributeValue("id", "").Remove(0, 1);
                    var pic = node
                        .Descendants("img").FirstOrDefault(x => x.GetAttributeValue("class", "")
                            .Contains("large-picture-img"));
                    var urlPicture = pic.Attributes["src"].Value;
                    // получим ссылку
                    var urlAndTitleHolder = node
                        .Descendants("div") // description
                        .FirstOrDefault(x => x.GetAttributeValue("class", "").Contains("description item_table-description"));
                    if (urlAndTitleHolder == null) continue;
                    {
                        var linkNode = urlAndTitleHolder
                            .Descendants("a")
                            .FirstOrDefault(x => x.GetAttributeValue("class", "").Contains("snippet-link"));
                        if (linkNode == null) continue;
                        {
                            var url = linkNode.GetAttributeValue("href", "");
                            // title
                            var title = linkNode.InnerText;

                            // get the price
                            var priceHolder = node
                                .Descendants("div") // description
                                .FirstOrDefault(x => x.GetAttributeValue("class", "").Contains("description item_table-description"));

                            if (priceHolder == null) continue;
                            {
                                var nodeWithPrice = priceHolder
                                    .Descendants("div")
                                    .FirstOrDefault(x => x.GetAttributeValue("class", "").Contains("about"));

                                var price = 0D;
                                if (nodeWithPrice?.ChildNodes != null && nodeWithPrice.ChildNodes.Any())
                                {
                                    nodeWithPrice = nodeWithPrice.ChildNodes[2];

                                    var priceChars = nodeWithPrice
                                        .InnerText
                                        .Where(char.IsDigit);
                                    var tmp = string.Join("", priceChars);
                                    double.TryParse(tmp, out price);
                                }

                                // if url is relative, add current url host at beginning
                                if (url.StartsWith("/"))
                                {
                                    var domainOriginal = new Uri(parentUrl);
                                    var domain = domainOriginal.GetLeftPart(UriPartial.Authority);
                                    url = domain + url;
                                }

                                // записываем всё что нашли в результат
                                result.Add(new DetailShort()
                                {
                                    UrlParent = parentUrl,
                                    WebID = id,
                                    Url = url,
                                    Title = title,
                                    Price = price,
                                    PublishDT = DateTime.Now,
                                    CurrencyCode = CurrencyCodes.RUB,
                                    UrlPicture = urlPicture
                                });
                            }
                        }
                    }
                }
            }
            return result;
        }
        public Detail GetUnitDetailsFromHtml(IDetailShort listUnit, string html)
        {
            if (listUnit == null)
                throw new ArgumentNullException("listUnit");
            if (string.IsNullOrEmpty(html))
                throw new ArgumentException("html is null or empty");

            // создадим новый элемент для вставки
            var result = new Detail();
            // зададим ссылку
            result.UrlParent = listUnit.UrlParent;
            result.Url = listUnit.Url;
            // дата. Примим текущую
            result.PublishDT = DateTime.Now;
            // задали ID
            result.WebID = listUnit.WebID;
            // создадим htmldocument
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            // получим Title
            var ttl = doc.DocumentNode
                .Descendants("h1").FirstOrDefault(x => x.GetAttributeValue("class", "") == "title-info-title"); // class title-info-title-text
            if (ttl != null)
            {
                result.Title = HttpUtility.HtmlDecode(ttl.InnerHtml);
            }
            // получим описание 
            var mainText = doc.DocumentNode
                .Descendants("div").FirstOrDefault(x => x.GetAttributeValue("itemprop", "") == "description"); //description
            if (mainText != null)
            {
                result.Content = "";
                var mainTextInner = mainText.Descendants("p").ToList();
                foreach (var n in mainTextInner)
                {
                    result.Content += HttpUtility.HtmlDecode(n.InnerHtml);
                }
            }
            // получим цену
            var priceNode = doc.DocumentNode
                .Descendants("span").FirstOrDefault(x => x.GetAttributeValue("class", "") == "price-value-string js-price-value-string"); // itemprop price
            if (priceNode?.ChildNodes != null && priceNode.ChildNodes.Any())
            {
                priceNode = priceNode.ChildNodes.First();
                var digits = priceNode.InnerHtml
                    .Where(char.IsDigit);
                var tmp = string.Join("", digits);
                int.TryParse(tmp, out var parsedPrice);
                if (parsedPrice != 0)
                    result.Price = parsedPrice;

            }
            // ссылки на картинки
            var picturesNodes = doc.DocumentNode.Descendants("div")
                .Where(x => x.GetAttributeValue("data-type", "") == "photo").ToList();
            if (picturesNodes.Count > 0)
            {
                foreach (var picNode in picturesNodes)
                {
                    var picANodes = picNode.Descendants("a");
                    var htmlNodes = picANodes as HtmlNode[] ?? picANodes.ToArray();
                    if (!htmlNodes.Any()) continue;
                    var pictureNode = htmlNodes.FirstOrDefault(x => x.GetAttributeValue("class", "") == "gallery-link");
                    if (pictureNode == null || pictureNode.Attributes.All(x => x.Name != "href")) continue;
                    {
                        var pictureAttributes = pictureNode.Attributes.Where(x => x.Name.ToLower() == "href");
                        var htmlAttributes = pictureAttributes as HtmlAttribute[] ?? pictureAttributes.ToArray();
                        if (!htmlAttributes.Any()) continue;
                        var ourPictureAttribute = htmlAttributes.First();
                        if (ourPictureAttribute == null || string.IsNullOrWhiteSpace(ourPictureAttribute.Value))
                            continue;
                        var item = ourPictureAttribute.Value;
                        result.PictureUrls.Add("http:" + item);
                    }
                }
            }
            else
            { // картинка может быть одна. Тогда предидущая часть не сработает. 
                var singlePicNode = doc.DocumentNode
                .Descendants("div").FirstOrDefault(x => x.GetAttributeValue("class", "")
                                                            .IndexOf("picture-aligner", StringComparison.Ordinal) >= 0);
                if (singlePicNode == null) return result;
                var singleNode = singlePicNode
                    .Descendants("img").FirstOrDefault(x => x.GetAttributeValue("itemprop", "") == "image");
                if (singleNode == null) return result;
                var singleImg = singleNode.GetAttributeValue("src", "");
                if (!string.IsNullOrEmpty(singleImg))
                    result.PictureUrls.Add("http:" + singleImg);
            }
            return result;
        }
    }
}
