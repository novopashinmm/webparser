﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreElements
{
    public interface IDetailShort
    {
        /// <summary>Database ID of the unit</summary>
        int ID { get; set; }

        /// <summary>Website ID of the unit</summary>
        string WebID { get; set; }

        /// <summary>The URL with list of links to details</summary>
        string UrlParent { get; set; }

        /// <summary>Current item URL</summary>
        string Url { get; set; }

        /// <summary>Price of the Unit</summary>
        double Price { get; set; }

        /// <summary>Currency code of the price</summary>
        CurrencyCodes CurrencyCode { get; set; }

        /// <summary>Title of the posting</summary>
        string Title { get; set; }

        /// <summary>Date and time when the item is posted online</summary>
        DateTime PublishDT { get; set; }

        /// <summary>This unit is processed by details parser</summary>
        bool IsParsed { get; set; }
    }
}
