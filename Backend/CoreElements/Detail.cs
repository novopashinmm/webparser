﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreElements
{
    public class Detail : IDetail
    {
        /// <summary>Content of the unit</summary>
        public string Content { get; set; }

        /// <summary>Currency code of the price</summary>
        public CurrencyCodes CurrencyCode { get; set; }

        /// <summary>Database ID</summary>
        public int ID { get; set; }

        /// <summary>this item is mailed</summary>
        public bool IsMailed { get; set; }

        /// <summary>This unit is parsed</summary>
        public bool IsParsed { get; set; }

        /// <summary>Lattitude of the unit</summary>
        public double Lattitude { get; set; }

        /// <summary>Longitude of the unit</summary>
        public double Longitude { get; set; }

        /// <summary>List of unit picures URLs</summary>
        public List<string> PictureUrls { get; set; }

        /// <summary>Price of the unit</summary>
        public double Price { get; set; }

        /// <summary>DateTime the unit is posted</summary>
        public DateTime PublishDT { get; set; }

        /// <summary>Title of the unit</summary>
        public string Title { get; set; }

        /// <summary>URL of the unit</summary>
        public string Url { get; set; }

        /// <summary>URL at which the link to the unit is found</summary>
        public string UrlParent { get; set; }

        /// <summary>website ID of the unit</summary>
        public string WebID { get; set; }

        /// <summary>Constructor</summary>
        public Detail()
        {
            this.PictureUrls = new List<string>();
        }
    }
}
