﻿using System;

namespace CoreElements
{
    public class DetailShort : IDetailShort
    {
        /// <summary>Currency code of the unit</summary>
        public CurrencyCodes CurrencyCode { get; set; }

        /// <summary>Database ID of the unit</summary>
        public int ID { get; set; }

        /// <summary>This unit is parsed</summary>
        public bool IsParsed { get; set; }

        /// <summary>Price of the unit</summary>
        public double Price { get; set; }

        /// <summary>Posting datetime</summary>
        public DateTime PublishDT { get; set; }

        /// <summary>Title of the unit</summary>
        public string Title { get; set; }

        /// <summary>Url of unit details</summary>
        public string Url { get; set; }

        /// <summary>URL of the page where this unit is found</summary>
        public string UrlParent { get; set; }

        /// <summary>ID of the unit at the web-site</summary>
        public string WebID { get; set; }
        public string UrlPicture { get; set; }
    }
}
