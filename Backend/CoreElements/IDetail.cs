﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreElements
{
    /// <summary>
    /// Interface for unit detailes taken from unit details page
    /// </summary>
    public interface IDetail
    {
        /// <summary>Database ID of the item</summary>
        int ID { get; set; }

        /// <summary>Website Add ID</summary>
        string WebID { get; set; }

        /// <summary>The URL with list of links to details</summary>
        string UrlParent { get; set; }

        /// <summary>Current item URL</summary>
        string Url { get; set; }

        /// <summary>Title of the unit</summary>
        string Title { get; set; }

        /// <summary>Date and time when the item is posted online</summary>
        DateTime PublishDT { get; set; }

        /// <summary>List of links to images of the add</summary>
        List<string> PictureUrls { get; set; }

        /// <summary>The price of the unit</summary>
        double Price { get; set; }

        /// <summary>Currency code of the price</summary>
        CurrencyCodes CurrencyCode { get; set; }

        /// <summary>Content of the add</summary>
        string Content { get; set; }

        /// <summary>Lattitude of the unit</summary>
        double Lattitude { get; set; }

        /// <summary>Longitude of the unit</summary>
        double Longitude { get; set; }

        /// <summary>This unit is processed by details parser</summary>
        bool IsParsed { get; set; }

        /// <summary>this item is mailed</summary>
        bool IsMailed { get; set; }
    }
}
