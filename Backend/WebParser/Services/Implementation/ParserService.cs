﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using CoreElements;
using WebParser.Services.Interfaces;
using AvitoPlugin;
using WebParserNew.Services.Helpers;

namespace WebParserNew.Services.Implementation
{
    public class ParserService : IParserService
    {
        private WebHelper _webHelp;
        private Avito _avito;
        private INotifierService _notifier;

        public ParserService(
            WebHelper webHelp, 
            Avito avito, INotifierService notifier)
        {
            _webHelp = webHelp;
            _avito = avito;
            _notifier = notifier;
        }

        public string Launch()
        {
            var url = "https://www.avito.ru/moskva_i_mo?q=%D0%BA%D0%BE%D0%BB%D0%BE%D0%BD%D0%BA%D0%B0+jbl";
            var htmlWithListOfItems = "";
            if (!_webHelp.GetHtmlFromUrl(url, out htmlWithListOfItems))
            {
                return "";
            }
            else
            {
                List<DetailShort> ListUnits = Avito.GetLinksFromListPage(htmlWithListOfItems, url).ToList();

                Console.WriteLine($"Количество вернувшихся записей {ListUnits.Count}");
                _notifier.Notify(ListUnits);

                var sb = new StringBuilder();
                foreach (var unit in ListUnits)
                {
                    sb.AppendLine($"Цена {unit.Price} {unit.CurrencyCode}");
                    sb.AppendLine($"Ссылка {unit.Url} ");
                    sb.AppendLine($"<img src={unit.UrlPicture}></img>");
                    sb.AppendLine("<br>--------------------------------------------------------");
                }

                return sb.ToString();
            }
        }
    }
}
