﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebParser.Services.Interfaces
{
    public interface IParserService
    {
        string Launch();
    }
}
