﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using CoreElements;

namespace WebParser.Services.Interfaces
{
    public interface INotifierService
    {
        void Notify(List<DetailShort> Units);
    }
}
