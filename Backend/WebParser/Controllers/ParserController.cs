using System;
using Microsoft.AspNetCore.Mvc;
using WebParser.Services.Interfaces;

namespace WebParserNew.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParserController : Controller
    {
        private readonly IParserService _parserService;
        public ParserController(IParserService parserService)
        {
            _parserService = parserService;
        }

        [HttpGet("run")]
        public ActionResult RunParse()
        {
            try
            {
                Console.WriteLine("Run запрос");
                var result = _parserService.Launch();
                return Ok(result);
            }
            catch (Exception e)
            {
                var messageError = e.Message;
                while (e.InnerException != null)
                {
                    messageError += $"\n {e.InnerException.Message}";
                    e = e.InnerException;
                }
                Console.WriteLine(messageError);
                throw;
            }
        }
    }
}