import { Injectable, Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

const API_URL = '/api/parser/run';

@Injectable()
export class HomeServiceComponent {

  constructor(
    private http: Http
  ) {
  }

  runParser(): Observable<Response> {
    return this.http.get(API_URL);
  }
}
