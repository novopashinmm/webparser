import { Component, OnInit } from '@angular/core';
import { HomeServiceComponent } from './home.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.styl']
})
export class HomeComponent {
  users: User[] = [];

  show = false;
  result: string;
  constructor(public _service: HomeServiceComponent, public userService: UserService)
  {
  }

  ngOnInit() {
    this.userService.getAll().pipe(first()).subscribe(users => {
        this.users = users;
    });
  }

  submit() {
    this._service.runParser().subscribe(x => this.result = x.text());
  }
}
