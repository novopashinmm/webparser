import { NgModule } from '@angular/core';
import { HomeComponent } from "./home.component";
import { homeRouter } from "./home.router";
import { CommonModule } from '@angular/common';
import { HomeServiceComponent } from './home.service';

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    homeRouter,
    CommonModule
  ],
  providers: [HomeServiceComponent],
})

export class HomeModule { }
