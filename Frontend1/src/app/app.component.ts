import { Component, OnInit } from '@angular/core';
import {ApiService} from './api.service';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  title = 'my-app';
  result: string;
  constructor(
    private apiService: ApiService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  submit() {
     this.apiService.runParser().subscribe(x => this.result = x.text());
  }
}
