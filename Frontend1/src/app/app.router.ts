import { ModuleWithProviders } from "@angular/compiler/src/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { AuthGuard } from "./guards/auth.guard";

export const router: Routes = [
  // path: 'home',
  // loadChildren: './home/home.module#HomeModule'
{
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard]
},
{
    path: 'login',
    component: LoginComponent
},

// otherwise redirect to home
{ path: '**', redirectTo: '' }
];

export const appRouter: ModuleWithProviders = RouterModule.forRoot(router);
