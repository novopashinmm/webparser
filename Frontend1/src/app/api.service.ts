import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

const API_URL = '/api/parser/run';

@Injectable()
export class ApiService {

  constructor(
    private http: Http
  ) {
  }

  runParser(): Observable<Response> {
    return this.http.get(API_URL);
  }
}
