Task default -depends DeployBackend

Task DeployBackend -description "Build and Push backend docker image to registry" {
    Push-Location -Path "./Backend"

    exec { docker build -q -t "webparser". }
    exec { docker tag "webparser" "novopashinmm/webparser" }
	exec { docker login --username $parameters.username --password $parameters.pass }
    exec { docker push "novopashinmm/webparser" }
	exec { docker pull "docker.io/novopashinmm/webparser" }
	if ($(docker ps -a -f name=webparser -q)) {
		exec { docker stop $(docker ps -a -f name=webparser -q) }
		exec { docker rm $(docker ps -a -f name=webparser -q) }
	}
	exec { docker run --name=webparser -d --restart=always -p 8088:80 webparser:latest }

    Pop-Location
}
